name: kizzle
dev_dependencies:
    - src: git https://github.com/Snektron/vulkan-zig.git
    - src: git https://github.com/Iridescence-Technologies/zglfw.git
      name: glfw
      main: src/main.zig