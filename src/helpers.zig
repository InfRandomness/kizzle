const vk = @import("vulkan");

pub const Ctx = struct {
    vkb: vk.BaseDispatch,
    vki: vk.InstanceDispatch,
    vkd: vk.DeviceDispatch,

    instance: vk.Instance,

    pub fn init() Ctx {
        const appInfo = vk.ApplicationInfo{
            .p_application_name = "Kizzle",
            .application_version = vk.makeApiVersion(0, 0, 0, 0),
            .p_engine_name = "Kizzle",
            .engine_version = vk.makeApiVersion(0, 0, 0, 0),
            .api_version = vk.API_VERSION_1_2,
        };

        self.instance = try vk.BaseDispatch.createInstance(.{ .flags = {}, .p_application_info = &appInfo, .enable_layer_count = 0, .pp_enabled_layer_names = undefined }, null);

        self.vk = InstanceDispatch.load();
    }
};
